
$(document).ready(function(){
	console.log($(document).scrollTop());

	//nav cambia de color cuando se baja la página
	$(document).scroll(function(){
		var about_me_top = $('#me_and_contact').offset().top;

		if ($(this).scrollTop() >= about_me_top){
			$("#header .cont-header nav").addClass("dark-nav-scroll");
			$("#header .cont-header nav").removeClass("light-nav-scroll");
		} else if ($(this).scrollTop() < about_me_top && $(this).scrollTop() > 90){
			$("#header .cont-header nav").addClass("light-nav-scroll");
			$("#header .cont-header nav").removeClass("dark-nav-scroll");
			$('.grid-title span').addClass("dark-span");
		} else if ($(this).scrollTop() <= 90){
			$("#header .cont-header nav").removeClass("light-nav-scroll");
			$('.grid-title span').removeClass("dark-span");
		}
	});


	//smoth scrolling
	$('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    	var target = $(this.hash);
			/*console.log(target[0].id);*/

	    	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    	if (target.length) {
		        $('html, body').animate({
		        	scrollTop: target.offset().top
		        }, 800);

		        return false;
	    	}
	    }
	});

	/*$('ul.nav').find('a').click(function (e) {
	    e.preventDefault();
	    var target = $(this).attr('href');
	    var $anchor = $(target).offset();
	    $('body').stop().animate({
	        scrollTop: $anchor.top
	    }, 'slow');
	});*/


	$('.nav-link, .navbar-brand').click(function(){
		$('#navbarNav').removeClass("show");
	});



	//  OVERLAY PORTAFOLIO
	proyect.forEach(function(element){
		$("#" + element.id).append('<div class="overlay"><div class="text"><span class="title">' + element.title.toUpperCase() + '</span><div class="cont-ellipsis"><p>' + element.description + '</p></div><ul><li><a id="a_seemore" href="#m_' + element.id + '" target="_blank" data-bs-toggle="modal" data-bs-placement="bottom" title="ver más"><i class="fas fa-ellipsis-h"></i></a></li><li><a id="a_code" href="' + element.code + '" target="_blank" data-bs-placement="bottom" title="proyecto en gitlab"><i class="fa fa-gitlab" aria-hidden="true"></i></a></li></ul></div></div>');

		
		$(".contenedor-modal").append('<div class="modal fade" id="m_' + element.id + '" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true"><div class="modal-dialog modal-dialog-centered"><div class="modal-content"><div class="modal-opacity"><div class="modal-header"><h3 class="modal-title" id="modalLabel">' + element.title + '</h3><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div><div class="modal-body"><p>' + element.description + '</p></div><div class="modal-footer"><ul id="modal-icons"><li><a id="a_code" href="' + element.code + '" target="_blank" data-bs-placement="bottom" title="proyecto en gitlab"><i class="fa fa-gitlab" aria-hidden="true"></i></a></li></ul></div></div></div></div></div>');


 /*style="background: ' + element.img + '; background-size: cover; background-position: center; background-repeat: no-repeat;"*/
		// var styles = '{"background": "url(../../dist/img/portfolio/' + element.img + ')", "background-size": "cover", "background-position": "center", "background-repeat": "no-repeat"}';

		// $(".modal-content").css(styles)

		/*$(".contenedor-modal").append('<div class="modal fade" id="modal-' + element.id + '" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true"><div class="modal-dialog  modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="modalLabel">' + element.title + '</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div class="row"><div class="col-sm-12 col-md-6 col-lg-6 col-xl-6"><div class="imgmod" id="imgmod_' + element.id + '"></div></div><div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">' + element.description + '</div></div><div class="row"><div class="cont-btn" style="margin: 30px auto;"><button type="button" class="btn btn-primary"><a href="' + element.code + '" target="_blank"><i class="fa fa-code" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Código"></i></a></button><button type="button" class="btn btn-primary"><a href="' + element.url + '" target="_blank"><i class="fa fa-github" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Visualización"></i></a></button></div></div></div></div></div></div>');*/

		
	});

	
	//  INICIALIZAR TOOLTIP
  	$('[data-bs-toggle="tooltip"]').tooltip();


})